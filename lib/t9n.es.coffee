T9n.map 'es',
  app:
    name: 'Que dirias si.. ?'
  titles:
    general: 'Que dirias si.. ?'
    notifications: 'Notificaciones'
  signin: "Entrar"
  signIn: "Entrar"
  signOut: "Salir"
  signout: "Salir"
  signUp: "Registrate"
  signup: "Registrate"
  OR: "O"
  forgotPassword: "Olvidaste tu Contraseña"
  emailAddress: "Dirección de Email"
  emailResetLink: "Reiniciar Email"
  dontHaveAnAccount: "No tenés una cuenta?"
  resetYourPassword: "Resetear tu contraseña"
  updateYourPassword: "Actualizar tu contraseña"
  password: "Contraseña"
  usernameOrEmail: "Usuario o email"
  email: "Email"
  ifYouAlreadyHaveAnAccount: "Si ya tenés una cuenta"
  signUpWithYourEmailAddress: "Registrate con tu email"
  username: "Usuario"
  optional: "Opcional"
  signupCode: "Codigo para suscribirse"
  clickAgree: "Si haces clic en Registrate estas de acuerdo con la"
  terms: "Politicas de Privacidad y Terminos de Uso"
  sign: "Ingresar"
  configure: "Configurar"
  with: "con"
  createAccount: "Crear cuenta"
  and: "y"
 
  error:
    minChar: "7 carácteres mínimo."
    pwOneLetter: "mínimo una letra."
    pwOneDigit: "mínimo un dígito."
    usernameRequired: "Usuario es obligatorio."
    emailRequired: "Email es obligatorio."
    signupCodeRequired: "Código para registrarse es obligatorio."
    signupCodeIncorrect: "Código para registrarse no coincide."
    signInRequired: "Debes iniciar sesión para hacer eso."
    usernameIsEmail: "Usuario no puede ser Email."

  links:
    privacy: 'Politicas de privacidad'
    terms: 'Politicas de Privacidad y Terminos de uso'
    allPosts: 'Todas las preguntas'
    myPosts: 'Mis preguntas'
    myCommentedPosts: 'Mis comentarios'
    myFavoritePosts: 'Mis preguntas favoritas'
    mostCommented: 'Los mas comentados'
    mostFavorites: 'Los preferidos'

  post:
    body: 'Que dirias si'
    too_short: 'El mensaje es muy corto, debe contener mas de 1 caracter...'
    comments: 'Comentario'
    edit: 'Editar'
    favorite: 'Favorito'
    remove: 'Eliminar'
    loading: 'Cargar mas mensajes...'
    submit: 'Enviar'
    show_post: 'Ir al mensaje completo'
    delete: 'Estas seguro que deseas eliminar la publicacion?'
    emptyBody: 'El mensaje esta vacio...'
    invalid: 'No pudimos identificar la publicacion que deseas comentar.'
  comments:
    too_short: 'El comentario es muy corto, debe contener mas de 1 caracter...'
    submit: 'Enviar'
  comment:
    remove: 'Eliminar'
    delete: 'Estas seguro que deseas eliminar el comentario?'
  notifications:
    title: 'Notificaciones'
    empty: 'No hay notificaciones'
    cannotRead: 'No se pudo leer...'
    subtitle1: 'Hay '
    subtitle2: ' comentarios en tu publicacion:'
  users:
    loginToVote: 'Debes entrar a tu cuenta para marcar como favorito.'
    needToLogin: 'Debes entrar a tu cuenta.'
    loginToPost: 'Debes entrar a tu cuenta para hacer publicaciones o comentar.'
    support: 'Soporte'
  share:
    share: 'Compartir en: '
    twitter: 'Compartir en twitter'
    facebook: 'Compartir en facebook'
    googleplus: 'Compartir en Google+'
