Router.configure({
  layoutTemplate: 'layout',
  yieldTemplates: {
    header: {
      to: 'menu'
    },
    footer: {
      to: 'footer'
    }
  },
  waitOn: function() {
    [Meteor.subscribe('notifications')]
  }
});

PostsController = RouteController.extend({
  template:  'posts',
  increment: function() {
    Session.set('postsIncrement', 10);
    return Session.get('postsIncrement');
  },
  limit: function() {
    limit = parseInt(this.params.postsLimit) || this.increment();
    Session.set('postsLimit', limit);
    return limit;
  },
  filter: {},
  findOptions: function() {
    try { sort = this.sort; } catch(err) { { createdAt: -1 } };
    try { limit = this.limit(); } catch(err) { 10 };
    return { sort: sort, limit: limit };
  },
  onBeforeAction: function() {
    Session.set('activePost', false);
  },
  data: function() {
    var hasMore = this.posts().count() === this.limit();
    return {
      posts: this.posts(),
      nextPath: hasMore ? this.nextPath() : null
    }
  }
});

DashboardController = PostsController.extend({
  sort: { createdAt: -1 },
  nextPath: function() {
    return Router.routes.dashboard.path({ postsLimit: this.limit() + this.increment() });
  },
  posts: function() {
    return Posts.find(this.filter, this.findOptions());
  },
  waitOn: function() {
    [Meteor.subscribe('posts', this.filter, this.findOptions)];
  }
});

FavoritesController = PostsController.extend({
  sort: { votesCount: -1 },
  nextPath: function() {
    return Router.routes.favorites.path({ postsLimit: this.limit() + this.increment() });
  },
  posts: function() {
    return Posts.find(this.filter, this.findOptions());
  },
  waitOn: function() {
    [Meteor.subscribe('posts', this.filter, this.findOptions)];
  }
});

MyFavoritesController = PostsController.extend({
  sort: { createdAt: -1, votesCount: -1 },
  nextPath: function() {
    return Router.routes.myfavorites.path({ postsLimit: this.limit() + this.increment() });
  },
  posts: function() {
    return Posts.find({ votes: Meteor.userId() }, this.findOptions());
  },
  waitOn: function() {
    [Meteor.subscribe('posts', { votes: Meteor.userId() }, this.findOptions)];
  }
});

MyPostsController = PostsController.extend({
  sort: { createdAt: -1 },
  nextPath: function() {
    return Router.routes.myposts.path({ postsLimit: this.limit() + this.increment() });
  },
  posts: function() {
    return Posts.find({ userId: Meteor.userId() }, this.findOptions());
  },
  waitOn: function() {
    [Meteor.subscribe('posts', { userId: Meteor.userId() }, this.findOptions)];
  }
});

Router.map(function() {
  this.route('terms', {
    path: '/terms-of-use',
    template: 'terms'
  });

  this.route('profile', {
    path: '/profile',
    onBeforeAction: function() {
      Router.go('/')
    }
  });

  this.route('post', {
    path: '/posts/:_id',
    template: 'show_post',
    waitOn: function() {
      [Meteor.subscribe('post', this.params._id),
        Meteor.subscribe('comments', this.params._id)];
    },
    onBeforeAction: function() {
      Session.set('activePost', true);
    },
    data: function() {
      return Posts.findOne({ _id: this.params._id });
    }
  });

  this.route('favorites', {
    path: '/favorites/:postsLimit?',
    controller: FavoritesController
  });

  this.route('myfavorites', {
    path: '/myfavorites/:postsLimit?',
    controller: MyFavoritesController
  });

  this.route('myposts', {
    path: '/myposts/:postsLimit?',
    controller: MyPostsController
  });

  this.route('postSubmit', {
    path: '/post/submit'
  });

  this.route('dashboard', {
    path: '/:postsLimit?',
    controller: DashboardController
  });

  this.route('dashboard', {
    path: '/dashboard/:postsLimit?',
    controller: DashboardController
  });
});

var requireLogin = function(pause) {
  if (! Meteor.user()) {
    if (Meteor.loggingIn())
      this.render(this.loadingTemplate);
    else
      this.render('accessDenied');

    pause();
  }
};

Router.onBeforeAction(requireLogin, {only: 'postSubmit'});
