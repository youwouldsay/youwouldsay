Meteor.startup(function () {
  Posts._ensureIndex({ _id: 1});
  Posts._ensureIndex({ userId: 1});
  Posts._ensureIndex({ createdAt: -1 });
  Posts._ensureIndex({ favoriteOf: 1 });
  Posts._ensureIndex({ votes: 1 });
  Posts._ensureIndex({ votesCount: -1 });
  Posts._ensureIndex({ commentsCount: -1 });
  Comments._ensureIndex({ postId: 1 });
  Notifications._ensureIndex({ userId: 1 });
});
