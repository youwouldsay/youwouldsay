if (keys = Meteor.settings['twitter-node-sdk']) {
  Twitter = new TwitMaker({
    consumer_key: keys.consumer_key,
    consumer_secret: keys.consumer_secret,
    access_token: keys.access_token,
    access_token_secret: keys.access_token_secret
  });
}

postToTwitter = function (post, postId) {
  if ( post && postId ) {
    message = post.body.slice(0, 60) + '...' + '@quediriassi http://quedirias.com' + Router.routes.post.path({_id: postId}) ;
    Twitter.post('statuses/update', { status: message }, function(err, data, response) {
      console.log(err);
    });
  }
}

fetchTweets = function () {
  var stream = Twitter.stream('statuses/filter', { track: 'quedirias, quediriassi'});

  stream.on('tweet', Meteor.bindEnvironment(function(tweet) {
    if (tweet.user.id !== 2580836335) {
      postId = Posts.insert({
        userId: null,
        body: tweet.text,
        createdAt: new Date().getTime(),
        commentsCount: 0,
        votesCount: 0,
        votes: []
      });

      // TODO: Refactor this
      message = tweet.text.slice(0, 60) + '...' + '@quediriassi http://quedirias.com' + Router.routes.post.path({_id: postId}) ;
      Twitter.post('statuses/update', { status: message }, function(err, data, response) {
        console.log(err);
      });
    }
  }));
};

Meteor.startup(function () {
  fetchTweets();
});
