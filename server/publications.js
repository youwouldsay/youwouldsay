Meteor.publish('posts',function (filter, options) {
  return Posts.find( filter, options);
});

Meteor.publish('post', function (postId) {
  return postId && Posts.find(postId);
});

Meteor.publish('comments', function (postId) {
  return Comments.find({ postId: postId });
});

Meteor.publish('notifications', function () {
  return Notifications.find({ userId: this.userId });
});
