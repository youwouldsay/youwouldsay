Comments = new Meteor.Collection('comments');

Comments.allow({
  remove: ownsDocument
});

Meteor.methods({
  newComment: function(commentAttributes) {
    this.unblock();
    var user = Meteor.user();
    var post = Posts.findOne(commentAttributes.postId);

    if (!user)
      throw new Meteor.Error(401, T9n('users.needToLogin'));

    if (!commentAttributes.body)
      throw new Meteor.Error(422, T9n.get('post.emptyBody'));

    if (!post)
      throw new Meteor.Error(422, T9n.get('post.invalid'));

    comment = _.extend(_.pick(commentAttributes, 'postId', 'body'), {
      userId: user._id,
      postId: post._id,
      body: commentAttributes.body,
      createdAt: new Date().getTime()
    });

    Posts.update(comment.postId, {$inc: {commentsCount: 1}});

    comment._id = Comments.insert(comment);

    createCommentNotification(comment);

    return comment._id;
  },

  removeComment: function(comment) {
    var user = Meteor.user();
    var post = Posts.findOne(comment.postId);

    if (!user)
      throw new Meteor.Error(401, T9n('users.needToLogin'));

    if (!post)
      throw new Meteor.Error(422, T9n.get('post.invalid'));

    Posts.update(comment.postId, {$inc: {commentsCount: -1}});
    Comments.remove({ _id: comment._id });
  }
});
