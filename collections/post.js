Posts = new Meteor.Collection('posts');

Posts.allow({
  update: ownsDocument,
  remove: ownsDocument
});

Posts.deny({
  update: function(userId, post, fieldNames) {
    return (_.without(fieldNames, 'body').length > 0);
  }
});

Meteor.methods({
  post: function(postAttributes) {
    var user = Meteor.user();

    if (!user)
      throw new Meteor.Error(401, T9n('users.needToLogin'));

    if (!postAttributes.body)
      throw new Meteor.Error(422, T9n.get('post.emptyBody'));

    var post = _.extend(_.pick(postAttributes, 'body'), {
      userId: user._id,
      createdAt: new Date().getTime(),
      commentsCount: 0,
      votesCount: 0,
      votes: []
    });

    var postId = Posts.insert(post);


    if(Meteor.isServer) {
      Future = Npm.require('fibers/future');
      var future = new Future();

      setTimeout(function () {
        future.return(postToTwitter(post));
        postToTwitter(post, postId);
      }, 3000);
    }

    return postId;
  },

  vote: function(postId) {
    var user = Meteor.user();
    if (!user)
      throw new Meteor.Error(401, T9n('post.loginToVote'));

    post = Posts.findOne(postId);
    if (_.contains(post.votes, user._id)) {
      Posts.update(
        { _id: postId },
        {
          $pull: {votes: user._id},
          $inc: {votesCount: -1}
        }
      );
    } else {
      Posts.update(
        { _id: postId },
        {
          $addToSet: {votes: user._id},
          $inc: {votesCount: 1}
        }
      );
    }
  },

  removePost: function(post) {
    var user = Meteor.user();

    if (!user)
      throw new Meteor.Error(401, T9n('users.needToLogin'));

    if (!post)
      throw new Meteor.Error(401, T9n('post.invalid'))

    if (post.userId !== user._id)
      throw new Meteor.Error(401, T9n('post.invalid'))

    Posts.remove({ _id: post._id});
    Comments.remove({ postId: post._id});
    Notifications.remove({ postId: post._id });
  }
});
