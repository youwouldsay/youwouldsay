Notifications = new Meteor.Collection('notifications');

Notifications.allow({
  update: ownsDocument
});

createCommentNotification = function(comment) {
  var post = Posts.findOne(comment.postId);
  if (comment.userId !== post.userId) {
    Notifications.insert({
      userId: post.userId,
      postId: post._id,
      commentId: comment._id,
      message: post.body.slice(0, 16) + '...',
      read: false
    });
  }
};

Meteor.methods({
  notified: function(postId) {
    var user = Meteor.user();

    if (!user)
      throw new Meteor.Error(401, T9n('users.needToLogin'));

    if (!postId)
      throw new Meteor.Error(422, T9n.get('post.emptyBody'));

    Notifications.update({ postId: postId, userId: user._id }, {$set: {read: true}}, { multi: true});
  }
});
