// Methods

// Template events

Template.new_comment.events({
  'keyup .body': function (e, t) {
    button = $(t.find('#comment-save'));
    input = $(t.find('.body')).val().length;
    if (input > 0) {
      button.removeClass('disabled');
    } else {
      if (!button.hasClass('disabled')) {
        button.addClass('disabled');
      }
    }
  },

  'submit form': function (e, t) {
    e.preventDefault();

    var $body = $(e.target).find('[name=body]');
    var comment = {
      body: $body.val(),
      postId: t.data._id
    };

    Meteor.call('newComment', comment, function(error, commentId) {
      if (error){
        throw(error.reason);
      } else {
        $body.val('');
      }
    });
  }
});
