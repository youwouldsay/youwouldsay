// Methods
Template.comments.helpers({
  comments: function() {
    post = this;
    return Comments({ postId: post._id }, { skip: post.comments().length-3, limit: 3, sort: { createdAt: 1}});
  }
});

// Template events
Template.comments.events({
  'click .full-post': function(e ,t) {
    post = this;
    Router.go('post', { postId: post._id});
  }
});
