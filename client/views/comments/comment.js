// Methods

Template.comment.helpers({
  createdAtFormatted: function () {
    moment(this.createdAt).fromNow();
  },

  ownComment: function () {
    return this.userId == Meteor.userId();
  }
});

// Template events

Template.comment.events({
  'click .comment-del': function (e, t) {
    e.preventDefault();
    comment = this;
    if (confirm(t9n('comment.delete'))) {
      $(t.firstNode).fadeOut(function() {
        Meteor.call('removeComment', comment)
      });
    }
  }
});
