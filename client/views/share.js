// Template Methods

Template.share.helpers({
  postUrl: function() {
    return 'http://quedirias.com' + Router.routes.post.path({_id: this._id});
  },

  tailedBody: function() {
    return this.body.slice(0, 60) + '...';
  }
});

// Template Events
