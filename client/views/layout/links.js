Template.links.events({
  'click #all': function (e, t) {
    Session.set('postsFilter', {});
    Session.set('postsOrder', { createdAt: -1 });
  },

  'click #my-posts': function (e, t) {
    Session.set('postsFilter', { user_id: Meteor.userId() });
    Session.set('postsOrder', { createdAt: -1 });
  },

  'click #my-favorite-posts': function (e, t) {
    Session.set('postsFilter', { votes: Meteor.userId() });
    Session.set('postsOrder', { createdAt: -1 });
  },

  'click #most-favorite-posts': function (e, t) {
    Session.set('postsFilter', { votesCount: { $exists: true} });
    Session.set('postsOrder', { votesCount: -1 });
  }
});
