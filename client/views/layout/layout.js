// Methods

Template.layout.helpers({
  rendered: function() {
    GAnalytics.pageview();
  }
});

// Template events

Template.layout.events({
  'click #i18n-en': function (e) {
    e.preventDefault();
    T9n.language = "es";
  },

  'click #i18n-es': function (e) {
    e.preventDefault();
    T9n.language = "es";
  }
});
