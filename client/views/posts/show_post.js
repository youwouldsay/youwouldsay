// Methods

Template.show_post.helpers({
  comments: function() {
    return Comments.find({ postId: this._id })
  }
});

// Template events
