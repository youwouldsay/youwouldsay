// Methods

// Template events

Template.new_post.events({
  'keyup #body': function (e, t) {
    button = $(t.find('#post-save'));
    input = $(t.find('#body')).val().length;
    if (input > 0) {
      button.removeClass('disabled');
    } else if (!button.hasClass('disabled')) {
      button.addClass('disabled');
    }
  },

  'submit form': function(e) {
    e.preventDefault();

    var post = {
      body: $(e.target).find('[name=body]').val(),
    };

    Meteor.call('post', post, function(error, id) {
      if (error) {
        throw(error.reason);

        if (error.error === 302)
          Router.go('post', {_id: error.details})
      } else {
        Router.go('post', {_id: id});
      }
    });
  }
});
