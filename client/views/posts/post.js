// Methods

Template.post.rendered = function () {
  Meteor.setTimeout(function () {
    $('.linkifable').linkify();
  }, 2000);
};

Template.post.helpers({
  ownPost: function() {
    return this.userId == Meteor.userId();
  },

  createdAtFormatted: function() {
    return moment(this.createdAt).fromNow();
  },

  commentsCount: function() {
    try {
      return this.commentsCount;
    } catch(err) {
      return 0;
    };
  },

  isVoted: function () {
    return _.contains(this.votes, Meteor.userId())
  },

  activePost: function() {
    return Session.get('activePost');
  }
});

// Template events

Template.post.events({
  'click .post-del': function (e, t) {
    e.preventDefault();
    var post = this;

    if (confirm(t9n('post.delete'))) {
      $(t.find('[name=post]')).fadeOut('slow', function() {
        Meteor.call('removePost', post);
      });
    }
  },

  'click .post-fav': function (e, t) {
    e.preventDefault();
    star = $(e.target).find('[name=star]');
    post = this;
    if (_.contains(this.votes, Meteor.userId())) {
      star.removeClass('glyphicon-star');
      star.addClass('glyphicon-star-empty');
    } else {
      star.removeClass('glyphicon-star-empty');
      star.addClass('glyphicon-star');
    }
    Meteor.call('vote', this._id);
  },
});
