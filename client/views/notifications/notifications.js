// Template methods

Template.notifications.helpers({
  notifications: function() {
    notifications = Notifications.find({userId: Meteor.userId(), read: false}).fetch();

    groupedPosts = _.groupBy(notifications, function(notification) {
      return notification.postId;
    });

    array = [];
    _.each(groupedPosts, function(value, key, list) {
      message = _.first(value).message;
      array.push({ postId: key, count: value.length, message: message})
    });

    return array;
  },

  notificationCount: function() {
    return Notifications.find({userId: Meteor.userId(), read: false}).count();
  }
});

Template.notification.helpers({
  notificationPostPath: function() {
    return Router.routes.post.path({_id: this.postId});
  },

  onPost: function() {
    try {
      return this.message;
    } catch(err) {
      return t9n('notifications.cannotRead');
    }
  }
});

// Template events

Template.notification.events({
  'click a': function() {
    Meteor.call('notified', this.postId);
  }
});
