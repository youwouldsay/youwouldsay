Handlebars.registerHelper('pluralize', function(n, thing) {
  // Pro stuff going on here...
  if (n === 1) {
    return '1 ' + T9n.get(thing);
  } else {
    return n + ' ' + T9n.get(thing) + 's';
  }
});
