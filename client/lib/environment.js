Meteor.startup(function() {
  T9n.language = "es";
  moment.lang("es");

  FlashMessages.configure = {
    autoHide: true,
    hideDelay: 3500,
    autoScroll: true
  };

  Accounts.config({ sendVerificationEmail: true });

  return AccountsEntry.config({
    logo: '',
    privacyUrl: '/privacy-policy',
    termsUrl: '/terms-of-use',
    homeRoute: '/',
    dashboardRoute: '/dashboard',
    profileRoute: 'profile',
    passwordSignupFields: 'EMAIL_ONLY',
    emailToLower: true,
    showSignupCode: false
  });

});
