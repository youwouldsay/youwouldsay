this.showMoreVisible = function() {
  threshold = $('#showMoreResults');
  target = $('#showMoreResults');

  if (!target.length) { return; };

  threshold = $(window).scrollTop() + $(window).height() - target.height();

  if (target.offset().top < threshold) {
    if (!target.data('visible')) {
      target.data('visible', true);
    } else {
      if (target.data('visible')) {
        target.data('visible', false);
        newLimit = Session.get('postsLimit') + Session.get('postsIncrement');
        Router.go(Router.current().route.name, { postsLimit: newLimit });
      }
    }
  }
};

$(window).scroll(function() {
  showMoreVisible();
});
